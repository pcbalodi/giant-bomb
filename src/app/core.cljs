(ns app.core
  "This namespace contains your application and is the entrypoint for 'yarn start'."
  (:require [reagent.dom :as rdom]
            [app.events]
            [app.subs]
            [app.router :as router]
            [app.views :refer [app]]
            [re-frame.core :refer [dispatch-sync]]))

(defn ^:dev/after-load render
      "Render the toplevel component for this app."
      []
      (rdom/render [app] (.getElementById js/document "app")))

(defn ^:export main
      "Run application startup logic."
      []
      (dispatch-sync [:init-db {}])
      (router/start!)
      (render))
