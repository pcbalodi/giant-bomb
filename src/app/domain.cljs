(ns app.domain)

(def cart-item-keys
  "Keys we store for games in cart"
  [:guid :name :description :image :deck])

(def page-size 10)

