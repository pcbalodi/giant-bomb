(ns app.events
  (:require
    [re-frame.core :refer [reg-event-db reg-event-fx reg-fx dispatch]]
    [ajax.core :refer [json-request-format json-response-format]]
    [day8.re-frame.http-fx]
    [clojure.string :as str]
    [app.domain :refer [cart-item-keys page-size]]
    [goog.string :as gstring]
    [goog.string.format]
    [app.utils :as utils]))

;; TODO Move this to config
(def api-url "https://www.giantbomb.com/api")
(def api-key "b2aa60d8b975d75a16dbd74cfa45129f9c1688f4")

(defn add-api-key-to-param [params]
      (merge params {:api_key api-key
                     :format  "jsonp"}))

(defn endpoint
      [& params]
      (str/join "/" (concat [api-url] params)))

;;;;;jsonp wrappers ;;;;
(defn ^:export get_games_success [response]
      (dispatch [:get-games-success (js->clj response :keywordize-keys true)]))

(defn ^:export get_game_success [response]
      (dispatch [:get-game-success (js->clj response :keywordize-keys true)]))

(def debounced-jsonp-callback (utils/debounce #(dispatch [:jsonp-callback]) 5000))

(reg-fx
  :jsonp-call
  (fn [{:keys [url]}]
      (debounced-jsonp-callback)
      (app.utils/make-jsonp-call url)))


;;;;;;Event Handlers;;;;;;;;;;;;;;;;;;;;
(reg-event-fx
  :init-db
  (fn [_ _]
      {:db {:games       []
            :cart        []
            :library     []
            :search-opts {:current-page 1
                          :query        ""
                          :total-pages  0}
            :loading?    false}}))


(reg-event-fx
  :set-active-page
  (fn [{:keys [db]} [_ {:keys [uri params] :as page}]]
      (let [dispatch-events (case uri
                                  :search (when (empty? (:games db))
                                                [[:get-games {}]])
                                  :game [[:get-game params]]
                                  [])]

           {:db         (merge db {:active-page page
                                   :show-cart?  false})
            :dispatch-n dispatch-events})))

(reg-event-fx
  :get-games
  (fn [{:keys [db]} [_ _]]
      (let [{:keys [query current-page]} (:search-opts db)
            handler-meta-tags (-> #'get_games_success meta)
            query-string (-> {:filter        (str "name:" query)
                              :limit         page-size
                              :offset        (* page-size (max (- current-page 1) 0))
                              :json_callback (str (:ns handler-meta-tags) "." (:name handler-meta-tags))}
                             add-api-key-to-param
                             app.utils/map->querystring)]
           {:jsonp-call {:url (gstring/format "%s?%s"
                                              (endpoint "games")
                                              query-string)}
            :db         (assoc db :loading? true)})))



(reg-event-fx
  :get-game
  (fn [{:keys [db]} [_ {:keys [guid] :as params}]]
      (if-let [game (first (filter #(= guid (:guid %)) (:games db)))]
              {:db (assoc db :selected-game game)}
              {:jsonp-call {:url (let [handler-meta-tags (-> #'get_game_success meta)]
                                      (gstring/format "%s?%s"
                                                      (endpoint "game" guid)
                                                      (-> {:json_callback (str (:ns handler-meta-tags) "." (:name handler-meta-tags))}
                                                          add-api-key-to-param
                                                          app.utils/map->querystring)))}
               :db         (assoc db :loading? true)})))

(reg-event-fx
  :add-to-cart
  (fn [{:keys [db]} [_ game]]
      {:db (update db :cart conj (select-keys game cart-item-keys))}))

(reg-event-fx
  :remove-from-cart
  (fn [{:keys [db]} [_ game]]
      {:db (update db :cart (fn [cart-games]
                                (remove #{(select-keys game cart-item-keys)} cart-games)))}))

(reg-event-fx
  :add-search-opts
  (fn [{:keys [db]} [_ opts]]
      (let [new-opts (merge (:search-opts db) opts)]
           {:db       (assoc db :search-opts new-opts)
            :dispatch [:get-games new-opts]})))

(reg-event-fx
  :toggle-show-cart
  (fn [{:keys [db]} [_ opts]]
      {:db (update db :show-cart? not)}))

(reg-event-fx
  :checkout
  (fn [{:keys [db]} []]
      {:db (-> db
               (update :library into (:cart db))
               (assoc :cart [])
               (merge {:notification {:show? true
                                      :text  "You have successfully checked out"
                                      :type  :info}}))}))

(reg-event-fx
  :hide-notification
  (fn [{:keys [db]} []]
      {:db (assoc db :notification {})}))

(reg-event-db
  :get-games-success
  (fn [db [_ response]]
      (-> db
          (assoc :loading? false)
          (assoc :games (:results response))
          (assoc-in [:search-opts :total-pages] (quot (:number_of_total_results response) page-size)))))

(reg-event-db
  :get-game-success
  (fn [db [_ response]]
      (-> db
          (assoc :loading? false)
          (assoc :selected-game (:results response)))))

(reg-event-fx
  :jsonp-callback
  (fn [{:keys [db]} []]
      (when (:loading? db)
        {:dispatch [:api-request-error]})))

(reg-event-db
  :api-request-error
  (fn [db [_ _]]
      (-> db
          (assoc :loading? false)
          (merge {:notification {:show? true
                                 :text  "Error getting data from backend"
                                 :type  :error}}))))


