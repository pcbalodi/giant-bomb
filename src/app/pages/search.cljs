(ns app.pages.search
  (:require [clojure.string :as str]
            [re-frame.core :refer [subscribe dispatch]]
            [app.utils :as utils]
            [app.router :as router]
            ["semantic-ui-react" :as ui]))

(defn- game-row [{:keys [aliases name deck platforms image original_game_rating guid] :as game}]
       [:> ui/Table.Row {:on-click #(router/push-url! router/game {:guid guid})}
        [:> ui/Table.Cell [:div [:img {:src   (:thumb_url image)
                                       :style {"width"      "100px"
                                               "height"     "100px"
                                               "object-fit" "cover"}}]
                           [:p name]]]
        [:> ui/Table.Cell deck]
        [:> ui/Table.Cell (utils/coll->str (map :name platforms) "N/A")]
        [:> ui/Table.Cell (utils/coll->str (map :name original_game_rating) "N/A")]])

(defn- game-list [games]
       (if (seq games)
        [:div.row
         [:div.twelve.wide.column
          [:> ui/Table {:celled     true
                        :padded     true
                        :selectable true}
           [:> ui/Table.Header
            [:> ui/Table.Row
             [:> ui/Table.HeaderCell "Title"]
             [:> ui/Table.HeaderCell "Summary"]
             [:> ui/Table.HeaderCell "Platforms"]
             [:> ui/Table.HeaderCell "Ratings"]]]
           [:> ui/Table.Body
            (map game-row games)]]]]
        [:div "No Games found matching query, please retry"]))

(defn- pagination [current-page total-pages]
       [:> ui/Pagination {:defaultActivePage current-page
                          :total-pages       total-pages
                          :on-page-change    #(dispatch [:add-search-opts {:current-page (.-activePage %2)}])}])

(defn page []
      (let [debounced-dispatcher (utils/debounce #(dispatch [:add-search-opts {:query %}]) 250)
            {:keys [total-pages current-page] :as search-opts} @(subscribe [:search-opts])]
       [:div.ui.padded.eight.column.centered.grid
        [:div.row
         [:div.eight.wide.column
          [:> ui/Input
           {:placeholder "Start typing name of a game..."
            :fluid       true
            :on-change   #(debounced-dispatcher (-> % .-target .-value))}]]]
        (when (pos? total-pages)
              (pagination current-page total-pages))
        (game-list @(subscribe [:games]))
        (when (pos? total-pages)
              (pagination current-page total-pages))]))