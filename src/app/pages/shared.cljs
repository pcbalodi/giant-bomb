(ns app.pages.shared
  (:require ["semantic-ui-react" :as ui]
            [app.router :as router]
            [re-frame.core :refer [subscribe dispatch]]))

(defn game-item-view [games]
      [:> ui/Item.Group
       (for [game games]
            [:> ui/Item
             [:a {:on-click #(app.router/push-url! router/game {:guid (:guid game)})} [:> ui/Item.Image {:size  "tiny"
                                                                                                         :style {"height" "50px"}
                                                                                                         :src   (get-in game [:image :thumb_url])}]]
             [:> ui/Item.Content
              [:> ui/Item.Header (:name game)]
              [:> ui/Item.Meta (:deck game)]
              [:div.ui.horizontal.divider]]])])

(defn library []
      (let [lib @(subscribe [:library])]
           [:div.ui.segment.centered.raised {:style {"width" "70%" "margin-left" "15%"}}
            [:h2.header "Your Games"]
            (if (seq lib)
              [:div.ui.container.sixteen.column.centered.padded.grid
               [:div.row
                [:div.eight.wide.column
                 (game-item-view lib)]]]
              [:div {:style {"width" "70%" "margin-left" "15%"}}
               "You dont have any games added yet. You can rent games and then they will appear here."])]))

(defn game []
      (let [{:keys [name image deck description guid] :as game-sub} @(subscribe [:selected-game])
            cart @(subscribe [:cart])
            library @(subscribe [:library])]
           [:<>
            [:div.container
             [:div.ui.padded.sixteen.column.centered.grid
              [:div.row
               [:div.four.wide.column
                [:h2.header name]
                [:> ui/Card
                 [:> ui/Image {:src (:medium_url image)}]
                 [:> ui/Card.Description deck]
                 ]
                (cond
                  (some #(app.utils/match-keys game-sub % [:guid]) library)
                  [:> ui/Label {:color "red" :fluid true :style {"width" "90%"}}
                   "You already own this game"]
                  (some #(app.utils/match-keys game-sub % [:guid]) cart)
                  [:> ui/Button {:color    "red" :fluid true :style {"width" "90%"}
                                 :on-click #(dispatch [:remove-from-cart game-sub])}
                   "Remove from cart"]
                  :else
                  [:> ui/Button {:color    "green" :fluid true :style {"width" "90%"}
                                 :on-click #(dispatch [:add-to-cart game-sub])}
                   "Add to cart"])]
               [:div.eight.wide.column
                [:div.container {:dangerouslySetInnerHTML {:__html description}}]]]]]]))

(defn checkout []
      (let [cart @(subscribe [:cart])]
           [:div.ui.segment.centered.raised {:style {"width" "70%" "margin-left" "15%"}}
            [:h2.header "Checkout Items in your cart"]
            (if (seq cart)
              [:div.ui.container.sixteen.column.centered.padded.grid
               [:div.row
                [:div.eight.wide.column
                 (game-item-view cart)]]
               [:div.row
                [:div.four.wide.column
                 [:> ui/Button {:primary  "true"
                                :on-click #(dispatch [:checkout {}])} "Place order"]]]]
              [:div
               "You dont have any games in cart."])]))

(defn message []
      (let [{:keys [type text show?] :as notification} @(subscribe [:notification])]
           (when show?
             [:> ui/Message {:info     (= :info type)
                            :negative (= :error type)
                             :on-dismiss #(dispatch [:hide-notification])}
             text])))