(ns app.router
  (:require [bide.core :as bide]
            [re-frame.core :refer [dispatch]]))

(def library :library)
(def search :search)
(def checkout :checkout)
(def game :game)

(def router
  (bide/router [["/library" library]
                ["/search" search]
                ["/checkout" checkout]
                ["/game/:guid" game]]))

(defn push-url! [k params]
      (bide/navigate! router k params))

(defn- on-navigate
       "A function which will be called on each route change."
       [name params query]
       (dispatch [:set-active-page {:uri    name
                                    :params params
                                    :query  query}]))


(defn start! []
      (bide/start! router {:default     :search
                           :on-navigate on-navigate}))

