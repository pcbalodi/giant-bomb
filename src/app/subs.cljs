(ns app.subs
  (:require [re-frame.core :refer [reg-sub]]))


(reg-sub
  :active-page
  (fn [db _]
      (:active-page db)))

(reg-sub
  :games
  (fn [db _]
      (:games db)))

(reg-sub
  :selected-game
  (fn [db _]
      (:selected-game db)))

(reg-sub
  :cart
  (fn [db _]
      (:cart db)))

(reg-sub
  :library
  (fn [db _]
      (:library db)))


(reg-sub
  :search-opts
  (fn [db _]
      (:search-opts db)))


(reg-sub
  :loading?
  (fn [db _]
      (:loading? db)))

(reg-sub
  :show-cart?
  (fn [db _]
      (:show-cart? db)))

(reg-sub
  :notification
  (fn [db _]
      (:notification db)))




