(ns app.utils
  "Bunch of utility functions"
  (:require [clojure.string :as str]
            [re-frame.core :refer [subscribe dispatch]])
  (:import [goog.async Debouncer]))


(defn coll->str
      "Remove nulls from a coll and then return it joined by , return alt text as fallback"
      ([coll]
       (coll->str coll ""))
      ([coll alt]
       (if-let [ncoll (seq (filter identity coll))]
               (str/join ", " ncoll)
               alt)))

(defn match-keys
      "Treat two maps as equal if the values
      for all keys in matcher-keys are equal"
      [m1 m2 matcher-keys]
      (= (select-keys m1 matcher-keys)
         (select-keys m2 matcher-keys)))


(defn map->querystring [m]
      (->> (map (fn [[k v]]
                (str (name k) "=" (js/encodeURIComponent v))) m)
           (str/join "&")))

(defn debounce [f interval]
      "Simple debouncer"
      (let [dbnc (Debouncer. f interval)]
           (fn [& args] (.apply (.-fire dbnc) dbnc (to-array args)))))

(defn make-jsonp-call [url-with-callback]
      (let [script-element (.createElement js/document "script") ]
           (set! (. script-element -src) url-with-callback)
           (.appendChild (.-body js/document) script-element)
           (.remove script-element)))

