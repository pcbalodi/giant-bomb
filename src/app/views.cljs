(ns app.views
  (:require [app.router :refer [search checkout game library]]
            [app.pages.search :as search-p]
            [app.router :as router]
            [app.pages.shared :as shared]
            [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as r]
            ["semantic-ui-react" :as ui]))


(defn header []
      (let [active-page @(subscribe [:active-page])
            cart @(subscribe [:cart])
            show-cart? @(subscribe [:show-cart?])]
           [:div
            [:div.header.center.aligned {:style {"margin-left" "45%"}}
             [:a {:on-click #(app.router/push-url! search {})} [:h2.header "Eight bomb"]]]
            [:div [:> ui/Menu
                   (for [menu-item-props [{:name     "Home"
                                           :active   (= active-page search)
                                           :on-click #(router/push-url! search {})}
                                          {:name     "My Games"
                                           :active   (= active-page library)
                                           :on-click #(router/push-url! library {})}
                                          {:name     (str "Cart " (if (seq cart) (str "(" (count cart) ")") ""))
                                           :active   false
                                           :on-click #(dispatch [:toggle-show-cart {}])
                                           :position "right"}]]
                        [:> ui/Menu.Item menu-item-props])]
             (when show-cart?
                   [:div {:style {"z-index"      "1000"
                                  "margin-right" "0.5%"
                                  "float"        "right"
                                  "position" "absolute"
                                  "right" "0%"
                                  "min-width" "20%"
                                  }}
                    [:div.ui.segment.raised
                     [:div.ui.horizontal.divider]
                     [:div.ui.padded.centerd.grid.two.column]
                     (if (seq cart)
                      (for [game cart]
                           [:div.row.raised
                            [:a {:on-click #(app.router/push-url! router/game {:guid (:guid game)})}
                             [:img {:src (get-in game [:image :tiny_url])}]]
                            [:span {:style {"margin-left" "2px"}} (:name game)]
                            [:div.ui.horizontal.divider]])
                      [:div "You dont have any games in cart"])
                     [:div.row.center.aligned {:style { "border-top" "1px solid black"}}
                      [:> ui/Button {:on-click #(router/push-url! router/checkout {})
                                     :primary true
                                     :disabled (empty? cart)
                                     :style {"margin-left" "30%" "margin-top" "10px"}} "Checkout!"]]]
                    [:div.ui.horizontal.divider]])]]))

(defonce page-mapping {game     shared/game
                       checkout shared/checkout
                       library  shared/library
                       search   search-p/page})

(defn app
      []
      (let [{:keys [uri] :as active-page} @(subscribe [:active-page])
            loading? @(subscribe [:loading?])]
           [:div
            [header]
            [shared/message]
            [:> ui/Loader {:active loading?}]
            ((get page-mapping uri search-p/page))]))
